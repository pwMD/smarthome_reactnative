'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image,
  ListView
} from 'react-native';

import GridView from 'react-native-grid-view'

var Navbar = require('../components/Navbar');
class SceneView extends Component{
  constructor(props){
    super(props);
    this.state = {
      dataSource: this.props.scenes
    };

console.log("GWT",this.props.scenes);

  }

  updatePowers(scenes){
    this.setState({
      dataSource: scenes
    });
  }

  renderRow(scene){
    return(
      <View style = {styles.row} key = {scene.scene_id}>
          <Text style = {styles.rowItemName} numberOfLines = {1}> {scene.name}</Text>
      </View>
    );
  }

  render(){
    var show = (this.state.dataSource == null || this.state.dataSource.length == 0) ? <Text style = {styles.noItems}> No  Scene </Text>
              : <GridView
                items={this.state.dataSource}
                itemsPerRow={2}
                renderItem={this.renderRow}
                style={styles.listView}/>;
    return(
      <View style = {styles.container}>
        <Navbar rootView = {this.props.rootView} />
        <View style = {styles.viewsContainer}>
          <Image style = {styles.firstView}
                 resizeMode = {Image.resizeMode.contain}>

            <Text style = {styles.titleText}>Scenes</Text>
            <View style = {styles.fakeBorder}/>
            <Image style = {styles.moduleImage}
                   resizeMode = {Image.resizeMode.stretch}
                   source = {require('../../images/aircon_background.png')}/>

             <View style = {styles.firstViewBottom}>
             <Text style = {styles.titleText}>Master Control</Text>

             </View>
             <Image style = {styles.switchImage}
                    resizeMode = {Image.resizeMode.contain}
                    source = {require('../../images/switch_on_white.png')}/>
          </Image>
          <View style = {styles.secondView}>
            <Text style = {styles.titleText}>Scene 01</Text>
            <View style = {styles.fakeBorder}/>
            <View style = {styles.listContainer}>
              {show}
            </View>
            <View style = {styles.fakeBorder}/>
            <View style = {styles.secondViewBottomButton}>
              <Image style = {styles.powerImage}
                     resizeMode = {Image.resizeMode.contain}
                     source = {require('../../images/power_button.png')}/>
              <Image style = {styles.switchImage}
                     resizeMode = {Image.resizeMode.contain}
                     source = {require('../../images/switch_on_white.png')}/>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex : 1,
    backgroundColor: '#F0EEED'
  },
  viewsContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'black'
  },
  firstView:{
    flex: 4,
    width: 30
  },
  secondView: {
    flex: 7,
    borderLeftWidth: 3,
    borderColor: 'gray'
  },
  titleText: {
    color: 'white',
    fontSize: 16,
    alignSelf: 'center',
    textAlign: 'center',
    margin: 8,
    backgroundColor: 'transparent'
  },
  fakeBorder:{
    backgroundColor: 'gray',
    height: 2,
    marginLeft: 5,
    marginRight:5
  },
  moduleImage: {
    width: 150,
    height: 200,
    alignSelf: 'center',
    margin: 50
  },
  firstViewBottom: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    marginTop: 60,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  switchImage:{
    width: 62,
    height: 62,
    alignSelf: 'center'
  },
  listContainer:{
    height: 180,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 10,
    marginBottom: 10
  },
  listView: {
    backgroundColor: 'black',
  },
  noItems: {
    flex : 8,
    fontSize: 16,
    textAlign: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingTop: 10
  },
  row: {
    height: 60,
    width: 90,
    margin: 5,
    backgroundColor: 'gray',
    alignItems: 'center',
    justifyContent: 'center'
  },
  rowItemName: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center'
  },
  secondViewBottomButton: {
    height: 110,
    width: 100,
    margin: 10,
    backgroundColor: 'gray',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end'
  },
  powerImage:{
    marginTop:15,
    width: 42,
    height: 42,
    alignSelf: 'center'
  },
});

module.exports = SceneView;
