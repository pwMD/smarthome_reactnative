'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image,
  Switch
} from 'react-native';

class MainView extends Component{
  constructor(props){
    super(props);
  }

  showView(view){
    this.props.rootView.showView(view);
  }

  render(){
    return(
      <View style = {styles.container}>

        <View style = {styles.halfContainer}>
          <TouchableHighlight style = {styles.subContainer}
                              underlayColor = 'transparent'
                              onPress = {this.showView.bind(this,4)}>
            <View style = {styles.box}>
              <View style={styles.commonTop}>
                <View style={styles.tempTextContainer}>
                  <Text style={styles.tempText}>28</Text>
                  <Text style={styles.tempLabel}>Room Temperature</Text>
                </View>
                <View style={styles.acControlContainer}>
                  <Text style={styles.btnPlusMinus}>+</Text>
                  <Text style={styles.tempControlText}>20</Text>
                  <Text style={styles.btnPlusMinus}>-</Text>
                </View>
              </View>
              <View style={styles.commonBottom}>
                <Image style = {styles.switchImage}
                       resizeMode = {Image.resizeMode.contain}
                       source = {require('../../images/switch_on_white.png')}/>
              </View>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style = {styles.subContainer}
                              underlayColor = 'transparent'
                              onPress = {this.showView.bind(this,2)}>
            <View style = {styles.box}>
              <View style={styles.commonTop}>
                <View style = {styles.commonBox}>
                  <Image style = {styles.commonImage}
                         resizeMode = {Image.resizeMode.contain}
                         source = {require('../../images/power_white.png')}/>
                  <Text style={styles.commonDeviceText}>Device 01</Text>
                </View>
              </View>
              <View style={styles.commonBottom}>
                <Image style = {styles.switchImage}
                       resizeMode = {Image.resizeMode.contain}
                       source = {require('../../images/switch_on_white.png')}/>
              </View>
            </View>
          </TouchableHighlight>

        </View>

        <View style = {styles.halfContainer}>
              <TouchableHighlight style = {styles.subContainer}
                                  underlayColor = 'transparent'
                                  onPress = {this.showView.bind(this,1)}>
                <View style = {styles.box}>
                  <View style={styles.commonTop}>
                    <View style = {styles.commonBox}>
                      <Image style = {styles.commonImage}
                             resizeMode = {Image.resizeMode.contain}
                             source = {require('../../images/light_white.png')}/>
                      <Text style={styles.commonDeviceText}>Device 01</Text>
                    </View>
                  </View>
                  <View style={styles.commonBottom}>
                    <Image style = {styles.switchImage}
                           resizeMode = {Image.resizeMode.contain}
                           source = {require('../../images/switch_on_white.png')}/>
                  </View>
                </View>
              </TouchableHighlight>

              <TouchableHighlight style = {styles.subContainer}
                                  underlayColor = 'transparent'
                                  onPress = {this.showView.bind(this,3)}>
                <View style = {styles.box}>
                  <View style={styles.commonTop}>
                    <View style = {styles.commonBox}>
                      <Image style = {styles.commonImage}
                             resizeMode = {Image.resizeMode.contain}
                             source = {require('../../images/curtain_icon_new.png')}/>
                      <Text style={styles.commonDeviceText}>Device 01</Text>
                    </View>
                  </View>
                  <View style={styles.commonBottom}>
                    <Image style = {styles.switchImage}
                           resizeMode = {Image.resizeMode.contain}
                           source = {require('../../images/switch_on_white.png')}/>
                  </View>
                </View>
              </TouchableHighlight>
        </View>




<View style = {styles.halfContainer}>
      <TouchableHighlight style = {styles.subContainer}
                          underlayColor = 'transparent'
                          onPress = {this.showView.bind(this,9)}>
        <View style = {styles.box}>
          <View style={styles.commonTop}>
            <View style = {styles.commonBox}>
              <Image style = {styles.commonImage}
                     resizeMode = {Image.resizeMode.contain}
                     source = {require('../../images/light_white.png')}/>
              <Text style={styles.commonDeviceText}>Scene 01</Text>
            </View>
          </View>
          <View style={styles.commonBottom}>
            <Image style = {styles.switchImage}
                   resizeMode = {Image.resizeMode.contain}
                   source = {require('../../images/switch_on_white.png')}/>
          </View>
        </View>
      </TouchableHighlight>

      <TouchableHighlight style = {styles.BlanksubContainer}
                          underlayColor = 'transparent'>
        <View style = {styles.box}>

        </View>
      </TouchableHighlight>
</View>


      </View>
    );
  }
}

let styles = StyleSheet.create({
  container:{
    flex: 1,
    padding:12,
    backgroundColor: 'black'
  },
  halfContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  subContainer: {
    flex: 1,
    borderRadius: 5,
    borderWidth: 3,
    margin: 3,
    borderColor: 'white'
  },
   BlanksubContainer: {
    flex: 1,
    borderRadius: 5,
    borderWidth: 3,
    margin: 3,
    borderColor: 'transparent'
  },
  box: {
    flex: 1
  },
  commonTop: {
    flex: 3,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  commonBottom: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  switch:{
    margin: 5
  },
  tempTextContainer:{
    alignItems: 'center',
    justifyContent: 'center',
    padding: 1,
    flex:4
  },
  tempText:{
    color: 'white',
    fontSize: 28,
    marginTop:40,
    fontWeight: 'bold'
  },
  tempLabel: {
    color: 'white',
    fontSize: 9,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:20,
    marginLeft:5
  },
  acControlContainer:{
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnPlusMinus: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center'
  },
  tempControlText: {
    color: 'gray',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  commonBox:{
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    flexDirection: 'row'
  },
  commonImage: {
    width: 58,
    height:58,
    alignSelf: 'center'
  },
  commonDeviceText: {
    color: 'white',
    fontSize: 8,
    marginLeft: 2,
    textAlign: 'center',
    alignSelf: 'center'
  },
  btNameContainer: {
    flex: 3,
    borderRadius: 5,
    borderWidth: 3,
    margin: 5,
    height: 30,
    borderColor: 'white'
  },
  btConnectionText: {
    flex: 6,
    borderRadius: 10,
    borderWidth: 3,
    margin: 3,
    height: 30,
    borderColor: 'white',
    justifyContent: 'center'
  },
  playerControlContainer:{
    flex:1,
    borderRadius: 5,
    borderWidth: 3,
    margin: 5,
    height: 28,
    borderColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  connectionText: {
    color: 'white',
    fontSize: 9,
    textAlign: 'center',
    alignSelf: 'center'
  },
  playerImage:{
    width: 20,
    height: 20,
    margin: 4,
    alignSelf: 'center'
  },
  switchImage:{
    width: 60,
    height: 38,
    margin: 2,
    alignSelf: 'center'
  },
  buttonCommon:{
    flex: 2,
    flexDirection: 'row',
  },
});

module.exports = MainView;
