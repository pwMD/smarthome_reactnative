'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image,
  AlertIOS
} from 'react-native';

import CheckBox from 'react-native-checkbox';

class LoginView extends Component{
  constructor(props){
    super(props);
    this.state = {
      email: 'sdroffice@schideron.com',
      password: '88888888'
    };
  }

  getFormBody(){
    var details = {
      'email': this.state.email,
      'password': this.state.password
    };

    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");
    return formBody;
  }



  _login(){
    this.props.rootView.showView(7);
      // this._checkNow();
  }




  _checkNow(){

          var data = new FormData();
          data.append('email',this.state.email);
          data.append('password',this.state.password);
          fetch('http://zencloud.perpetualwave.com/api/v1/user_login.json', {
              method: 'POST',
              body: data
          })
          .then((response) => response.json())
          .then((json) => {
            this._authenticate(json);
            console.log('ASDD: ',json);
          })
          .catch((err) => {
            alert(err);
          });
  }



  _authenticate(json){
    if(json){
      if(json.hasOwnProperty('settings')){
        var settings = json.settings[0];
        var settingJSON = JSON.parse(settings.settings);
        var rooms = settingJSON.rooms;


        var lights = [];
        var curtains = [];
        var aircons = [];
        var scenes = [];

        for(var x = 0; x < rooms.length; x++){
          var roomData = rooms[x].room_data;
          for(var y = 0 ; y < roomData.lighting_settings.length; y++){
            lights.push(roomData.lighting_settings[y]);
          }


          for(var y = 0 ; y < roomData.curtain_settings.length; y++){
            curtains.push(roomData.curtain_settings[y]);
          }



          for(var y = 0 ; y < roomData.aircon_settings.length; y++){
            aircons.push(roomData.aircon_settings[y]);
          }


          for(var y = 0 ; y < roomData.scene_settings.length; y++){
            scenes.push(roomData.scene_settings[y]);
          }


        }
        //alert(lights.length);
        // for(var x = 0; x < lights.length; x++){
        //   alert(lights[x].name);
        // }
        var pi = json.pis[0].sdrid;
        //alert(pi);

        this.props.rootView.setCurtains(curtains);
        this.props.rootView.setLights(lights);
        this.props.rootView.connectSocket(pi);
        this.props.rootView.setAircons(aircons);
        this.props.rootView.setScenes(scenes);
      }
    }
  }



  render(){
    return(
      <View style = {styles.container}>


         <Image style={styles.mark} source={require('../../images/sh_sample.png')} />

        <View style={styles.border}>
          <TextInput style = {styles.input}
                     placeholder = "Email"
                     onChangeText = {(text) => {this.setState({email: text})}}
                     value = {this.state.email}/>
          </View>


          <TouchableHighlight style = {styles.btn}
                              underlayColor ='transparent'
                              onPress = {this._login.bind(this)}>
            <Text style = {styles.btnText}>LOGIN</Text>
          </TouchableHighlight>
        </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    backgroundColor: '#000000',
        alignItems: 'center'
  },border:{
    borderRadius: 8,
    borderWidth:2,
      borderColor: 'white',
      margin:10
  },
  appImage: {
    width: 185,
    height: 185,
    alignSelf: 'center',
    marginBottom: 25
  },
  input: {
    width: 300,
    height: 40,
    borderRadius: 8,
    borderWidth: 1,
    padding: 8,
    alignSelf: 'center',
    backgroundColor: 'white'
  },
  btn:{
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: '#000000',
    borderRadius: 7,
    borderColor: 'white',
    borderWidth: 2,
    margin:20,
    width: 300,
    height: 48
  },
  btnText:{
    margin:5,
    textAlign:'center',
    alignSelf: 'center',
    color: 'white',
    fontSize: 24,
  },
  checkbox: {
    alignItems: 'center'
  },  mark: {
        width: 128,
        height: 128,
        margin:60
    },
});

module.exports = LoginView;
