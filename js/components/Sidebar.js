'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image,
  ScrollView

} from 'react-native';

class Sidebar extends Component{
  constructor(props){
    super(props);
  }

  showView(view){
    this.props.rootView.showView(view);
    this.props.rootView.toggleDrawer(false);
  }

  render(){
    return(
      <ScrollView style = {styles.container}>
        <View style = {styles.rowContainer}>
          <TouchableHighlight underlayColor = 'transparent'
                            onPress = {() => this.showView(4)}>
            <View style = {styles.subContainer}>
              <Image style = {styles.commonImage}
                     resizeMode = {Image.resizeMode.contain}
                     source = {require('../../images/fan_white.png')}/>
               <Text style = {styles.title}>AC</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight underlayColor = 'transparent'
                              onPress = {() => this.showView(2)}>
            <View style = {styles.subContainer}>
              <Image style = {styles.commonImage}
                     resizeMode = {Image.resizeMode.contain}
                     source = {require('../../images/power_white.png')}/>
               <Text style = {styles.title}>Power</Text>
            </View>
          </TouchableHighlight>
        </View>
        <View style = {styles.rowContainer}>
          <TouchableHighlight underlayColor = 'transparent'
                                onPress = {() => this.showView(3)}>
            <View style = {styles.subContainer}>
              <Image style = {styles.commonImage}
                     resizeMode = {Image.resizeMode.contain}
                     source = {require('../../images/curtain_icon_new.png')}/>
               <Text style = {styles.title}>Curtain</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight underlayColor = 'transparent'
                              onPress = {() => this.showView(1)}>
            <View style = {styles.subContainer}>
              <Image style = {styles.commonImage}
                     resizeMode = {Image.resizeMode.contain}
                     source = {require('../../images/light_white.png')}/>
               <Text style = {styles.title}>Lights</Text>
            </View>
          </TouchableHighlight>
        </View>
        <View style = {styles.rowContainer}>
          <TouchableHighlight underlayColor = 'transparent'
                              onPress = {() => this.showView(1)}>
            <View style = {styles.subContainer}>
              <Image style = {styles.commonImage}
                     resizeMode = {Image.resizeMode.contain}
                     source = {require('../../images/scene_menu_icon_93.png')}/>
               <Text style = {styles.title}>Scenes</Text>
            </View>
          </TouchableHighlight>

     <TouchableHighlight underlayColor = 'transparent'
                          onPress = {() => this.showView(8)}>
        <View style = {styles.subContainer}>
          <Image style = {styles.commonImage}
                 resizeMode = {Image.resizeMode.contain}
                 source = {require('../../images/menu_logout_93.png')}/>
           <Text style = {styles.title}>Logout</Text>
        </View>
      </TouchableHighlight>




        </View>

      </ScrollView>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    backgroundColor: 'black'
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  subContainer: {
    flex: 1,
    width: 85,
    height: 85,
    margin: 10,
    backgroundColor: 'gray',
    alignItems: 'center',
    justifyContent: 'center'
  },
  commonImage: {
    width: 55,
    height: 55,
    alignSelf: 'center'
  },
  title:{
    fontSize: 17,
    color: 'white',
    alignSelf: 'center',
    marginTop: 5
  }
});

module.exports = Sidebar;
