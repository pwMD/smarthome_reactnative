import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  AlertIOS,

} from 'react-native';


import BarcodeScanner from 'react-native-barcodescanner';
var context;
class QRCODEANDROID extends Component {
  constructor(props) {
    super(props);

    context = this;
    this.state = {
      torchMode: 'off',
      cameraType: 'back',
    };
  }




  barcodeReceived(verifcationCode) {

   if(verifcationCode !== null){
    context.props.rootView.showView(6);


     console.log('Barcode: ' + verifcationCode.data);
     console.log('Type: ' + verifcationCode.type);

     //  alert('Response Data: '+ verifcationCode.data  +  ' Type: ' +  verifcationCode.type);
      context._checkNow();
   }
}


_checkNow(){

        var data = new FormData();
        data.append('email','sdroffice@schideron.com');
        data.append('password','88888888');
        fetch('http://zencloud.perpetualwave.com/api/v1/user_login.json', {
            method: 'POST',
            body: data
        })
        .then((response) => response.json())
        .then((json) => {
          context._authenticate(json);
        })
        .catch((err) => {
          alert(err);
        });
}



_authenticate(json){
  if(json){
    if(json.hasOwnProperty('settings')){
      var settings = json.settings[0];
      var settingJSON = JSON.parse(settings.settings);
      var rooms = settingJSON.rooms;


      var lights = [];
      var curtains = [];
      var aircons = [];

      for(var x = 0; x < rooms.length; x++){
        var roomData = rooms[x].room_data;
        for(var y = 0 ; y < roomData.lighting_settings.length; y++){
          lights.push(roomData.lighting_settings[y]);
        }


        for(var y = 0 ; y < roomData.curtain_settings.length; y++){
          curtains.push(roomData.curtain_settings[y]);
        }



        for(var y = 0 ; y < roomData.aircon_settings.length; y++){
          aircons.push(roomData.aircon_settings[y]);
        }


      }
      //alert(lights.length);
      // for(var x = 0; x < lights.length; x++){
      //   alert(lights[x].name);
      // }
      var pi = json.pis[0].sdrid;
      //alert(pi);

      context.props.rootView.setCurtains(curtains);
      context.props.rootView.setLights(lights);
      context.props.rootView.connectSocket(pi);
      context.props.rootView.setAircons(aircons);
    }
  }
}


  render() {
    return (
      <BarcodeScanner
        onBarCodeRead={this.barcodeReceived}
        style={{ flex: 1 }}
        torchMode={this.state.torchMode}
        cameraType={this.state.cameraType}
      />
    );
  }
}


module.exports = QRCODEANDROID;
