 'use strict'
 import React, {Component} from 'react';
 import {
   ActivityIndicator,
   StyleSheet,
   View,
   AppRegistry
 } from 'react-native';


 const TimerMixin = require('react-timer-mixin');
 var context;

 const LoadingView = React.createClass({
   mixins: [TimerMixin],
   getInitialState() {
     return {
       animating: true,
     };
   },

   setToggleTimeout() {
     this.setTimeout(() => {
       this.setState({animating: !this.state.animating});
       this.setToggleTimeout();
     }, 8000);
   },


   _checkNow(){

           var data = new FormData();
           data.append('email','sdroffice@schideron.com');
           data.append('password','88888888');
           fetch('http://zencloud.perpetualwave.com/api/v1/user_login.json', {
               method: 'POST',
               body: data
           })
           .then((response) => response.json())
           .then((json) => {
             this._authenticate(json);
           })
           .catch((err) => {
             alert(err);
           });
   },



   _authenticate(json){
     if(json){
       if(json.hasOwnProperty('settings')){
         var settings = json.settings[0];
         var settingJSON = JSON.parse(settings.settings);
         var rooms = settingJSON.rooms;


         var lights = [];
         var curtains = [];
         var aircons = [];

         for(var x = 0; x < rooms.length; x++){
           var roomData = rooms[x].room_data;
           for(var y = 0 ; y < roomData.lighting_settings.length; y++){
             lights.push(roomData.lighting_settings[y]);
           }


           for(var y = 0 ; y < roomData.curtain_settings.length; y++){
             curtains.push(roomData.curtain_settings[y]);
           }



           for(var y = 0 ; y < roomData.aircon_settings.length; y++){
             aircons.push(roomData.aircon_settings[y]);
           }


         }
         //alert(lights.length);
         // for(var x = 0; x < lights.length; x++){
         //   alert(lights[x].name);
         // }
         var pi = json.pis[0].sdrid;
         //alert(pi);

         this.props.rootView.setCurtains(curtains);
         this.props.rootView.setLights(lights);
         this.props.rootView.connectSocket(pi);
         this.props.rootView.setAircons(aircons);
       }
     }
   },



   componentDidMount() {
     context = this;
     this.setToggleTimeout();
     this._checkNow();

   },

   render() {
     return (
      <View style={styles.gray}>

       <ActivityIndicator
         animating={this.state.animating}
         style={[styles.centering, {height: 32}]}
         size="large"
       />

      </View>
     );
   }
 });



 const styles = StyleSheet.create({
   centering: {
     alignItems: 'center',
     justifyContent: 'center',
     margin:200
   },
   gray: {
     backgroundColor: '#000000',
     flex:1,
     flexDirection:'column',

   },
 });


module.exports = LoadingView;
