'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableHighlight
} from 'react-native'

class Navbar extends Component{
  constructor(props){
    super(props);
  }

  render(){
    return(
      <View style={styles.container}>
        <TouchableHighlight style = {styles.btn}
        underlayColor='transparent'
        onPress = {() => this.props.rootView.toggleDrawer(true)}>
          <Image style = {styles.img} source = {{uri : 'http://www.actorslaunchpad.com/media/images/hamburger-icon.png'}} />
        </TouchableHighlight>

        <Text style = {styles.title}>{this.props.title}</Text>

        <TouchableHighlight style = {styles.btn}
        underlayColor='transparent'
        onPress = {() => this.props.rootView.showView(0)}>
          <Image style= {styles.img} source = {require('../../images/home_white.png')} />
        </TouchableHighlight>
      </View>
    );
  }

}

Navbar.defaultProps = {
  // leftButton: {
  //   img: '',
  //   onPress: () => {}
  // },
  // rightButton: {
  //   img: '',
  //   onPress: () => {}
  // }
}

Navbar.propTypes = {
  // leftButton: React.PropTypes.object,
  // rightButton: React.PropTypes.object,
  //rootView: React.PropTypes.object.isRequired
}

var styles = StyleSheet.create({
  container:{
    paddingTop:15,
    paddingLeft:10,
    paddingRight:10,
    height: 45,
    flexDirection: 'row',
    backgroundColor: 'black',
    borderBottomWidth: 3,
    borderColor: 'gray'

  },
  btn: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    flex: 10,
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 18,
    color: 'white'
  },
  img:{
    width: 24,
    height: 24,
    margin:5,
    alignSelf: 'center',
    justifyContent: 'center'
  }
});

module.exports = Navbar;
