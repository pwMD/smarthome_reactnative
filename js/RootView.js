'use strict'
import React, { Component } from 'react';
import {
  Navigator
} from 'react-native';
import Drawer from 'react-native-drawer'
if (!window.navigator.userAgent) {
    window.navigator.userAgent = "react-native";
}
var io = require('socket.io-client/socket.io');
var socket;

var context;
var Sidebar = require('./components/Sidebar');
var LoginView = require('./views/LoginView');
var LightsView = require('./views/LightsView');
var PowerView = require('./views/PowerView');
var MainView = require('./views/MainView');
var CurtainView = require('./views/CurtainView');
var AirconView = require('./views/AirconView');
var AlarmView = require('./views/AlarmView');
var AndroidQRView = require('./components/QRCODEANDROID');
var LoadingView = require('./components/LoadingView');
var SceneView = require('./views/SceneView');

class RootView extends Component{
  constructor(props){
    super(props);
    var lights = [];
    var curtains = [];
    var aircons = [];
    var scenes = [];
    context = this;
    this.state = {
      connected: false,
      status: ''
    }
  }

  setLights(lights){
    this.lights = lights;
  }


  setCurtains(curtains){
    this.curtains = curtains;
  }


  setAircons(aircons){
    this.aircons = aircons;
  }

  setScenes(scenes){
    this.scenes = scenes;
  }



  connectSocket(status){
    var socketAddress = 'http://zencloud.perpetualwave.com:5001';
    socket = io.connect(socketAddress, {jsonp: false, transports: ['websocket']});
    //alert('Connecting');
    socket.on('connect', function(data){
      context.setState({
        connected: true,
        status: status
      });
      socket.emit('mobileLogin', status);
      socket.emit('getPiStatus', status);
      context.showView(0);//MAIN VIEW
    });
    socket.on('disconnect', function(data){
      alert('disconnected');
      context.setState({
        connected: false,
        status: ''
      });
    });
    socket.on('connect_failed', function (data) {
        alert('NOT connected: ' + data);
    });
    // socket.on('updateStatus', function(data){
    //   context.updateStatus(data);
    // });
  }

  emitParams(params){
    if(this.state.connected){
      socket.emit('sendCommand', this.state.status, params);
    }
  }

  // updateStatus(data){
  //   var tLights = data;
  //   for(var x = 0 ; x < tLights.length; x++){
  //     if(tLights[x].hasOwnProperty('status')){
  //       this.setLightStatus(tLights[x].device_id, tLights[x].channel, tLights[x].status);
  //     }
  //   }
  // }

  setLightStatus(device_id, channel, status){
    for(var x = 0 ; x < this.lights.length; x++){
      if(this.lights[x].device_id == device_id && this.lights[x].channel == channel){
        if(this.lights[x].type == 0){
          this.lights[x].isOn = (status == 1);
        }else{
          this.lights[x].percent = (status * 100) / 255;
        }
      }
    }
  }

  showView(type){
    var component = MainView
    var passProps = {rootView: context};

    if(type == 0){
      component = MainView;
      passProps = {
        rootView: context
      }

    }else if(type == 1){//LIGHTS
      component = LightsView;
      passProps = {
        lights: context.lights,
        rootView: context
      }
    }else if(type == 2){//POWER
      component = PowerView;
      passProps = {
        powerSockets: [{name : 'Power 1'}],
        rootView: context
      }
    }else if(type == 3){ //CURTAIN
        component = CurtainView;
        passProps = {
        curtains: context.curtains,
        rootView: context
        }
    }else if(type == 4){ //AIRCON
      component = AirconView;
      passProps = {
      aircons: context.aircons,
      rootView: context
      }
    }else if(type == 5){ //ALARM
      component = AlarmView;
        passProps = {
        alarms:[{name:'Alarm 1'}],
        rootView: context
      }
    }else if(type == 6){
      component = LoadingView;
        passProps = {
          rootView: context
        }
    }else if(type == 7){
        component = AndroidQRView;
        passProps = {
           rootView: context
        }
    }else if(type == 8){
      component = LoginView;
      passProps = {
         rootView: context
      }
    }else if(type == 9){
      component = SceneView;
      passProps = {
         scenes: context.scenes,
         rootView: context
      }
    }






    context.navigator.replace({
      component: component,
      passProps: passProps
    })
  }

  toggleDrawer(isOpen){
    if(isOpen){
      this.drawer.open();
    }else{
      this.drawer.close();
    }
  }

  renderScene(route, navigator){
    return React.createElement(route.component, { ...this.props, ...route.passProps, navigator } )
  }

  configureScene(route, routeStack){
   return {...Navigator.SceneConfigs.HorizontalSwipeJump, gestures: false}
  }

  render(){
    return (
      <Drawer
        style = {{flex: 1}}
        ref={(ref) => this.drawer = ref}
        content={<Sidebar rootView = {context}/>}
        openDrawerOffset={160}>
        <Navigator
          configureScene={ this.configureScene }
          style = {{flex: 1}}
          initialRoute = {{component: LoginView, passProps: {rootView: this}}}
          renderScene = {this.renderScene.bind(this)}
          ref = {(ref) => this.navigator = ref}
          />
      </Drawer>
    );
  }
}

module.exports = RootView;
