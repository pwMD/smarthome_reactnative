'use strict'
import {AppRegistry} from 'react-native';

var RootView = require('./js/RootView');
AppRegistry.registerComponent('SmartHome', () => RootView);
